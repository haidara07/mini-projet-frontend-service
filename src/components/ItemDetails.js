import axios from 'axios';
import React, { useContext, useState } from 'react';
import { useParams } from 'react-router-dom';
import { UidContext } from './AppContext';

const ItemDetails = () => {
    const [quantity, setquantity] = useState(1);
    const [product, setProduct] = useState();
    const parametres = useParams();
    const uid = useContext(UidContext);

    axios({
        method: "get",
        url:`${process.env.REACT_APP_API_URL}/product/${parametres.productId}`,
        withCredentials: true,
    })
    .then((res) => {
        setProduct(res.data.product)
    })
    .catch((err) => console.log(err));

    const handleOrder = () => {
        if (uid) {
            axios({
                method: "post",
                url: `${process.env.REACT_APP_API_URL}/order/new`,
                withCredentials: true,
                data: {
                    totalPrice:product.price*quantity,
                    quantity:quantity,
                    productId: product._id,
                    userId:uid
                },
            })
            .then((res) => {
                console.log(res.data.order._id)
                window.location = `/commande/${res.data.order._id}`;
            })
            .catch((err) => {
                console.log(err);
            })  
            
        }else{
            window.location="/login"
        }
        
    }

    return (
        <>   
            {/* <!-- Page Header Start -->*/}
            <div class="container-fluid bg-secondary mb-5">
                <div class="d-flex flex-column align-items-center justify-content-center" style={{minHeight: '100px'}}>
                    <h1 class="font-weight-semi-bold text-uppercase mb-3">Shop Detail</h1>
                </div>
            </div>
            {/*<!-- Page Header End -->*/}
          
            {/*   <!-- Shop Detail Start -->*/}
            {product &&

            <div class="container-fluid py-5">
                <div class="row px-xl-5">
                    
                    <div class="col-lg-5 pb-5">
                        <div id="product-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner border">
                                <div class="carousel-item active">
                                     {/* <img class="w-100 h-100" src="/img/product-1.jpg" alt="img"/>*/}
                                     <img class="w-100 h-100" src={product.images} alt="img"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-7 pb-5">
                        <h3 class="font-weight-semi-bold">{product.name}</h3>
                        <h3 class="font-weight-semi-bold mb-4">${product.price}</h3>
                        <p class="mb-4">{product.description}</p>
                        
                        <div class="d-flex align-items-center mb-4 pt-2">
                            {/* <div class="input-group quantity mr-3" style="width: 130px;"> */}
                            <div class="input-group quantity mr-3" style={{width: '130px'}}>
                                <div class="input-group-btn">
                                    <button 
                                        class="btn btn-primary btn-minus"
                                        onClick={()=>{
                                            if(quantity===1){
                                                setquantity(1)
                                            }else{
                                                setquantity(quantity-1)
                                            }  
                                        }}
                                    >
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <input 
                                    type="text" 
                                    class="form-control bg-secondary text-center" 
                                    value={quantity}
                                />
                                <div class="input-group-btn">
                                    <button 
                                        class="btn btn-primary btn-plus" 
                                        onClick={()=>{
                                            if(quantity===9){
                                                setquantity(1)
                                            }else{
                                                setquantity(quantity+1)
                                            }  
                                        }}
                                    >
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <button class="btn btn-primary px-3" onClick={handleOrder}>Commander</button>
                        </div>
                    
                    </div>
                </div>
                
            </div>
            
            } 

            {/* <!-- Shop Detail End -->  */}
        </>
    );
};

export default ItemDetails;