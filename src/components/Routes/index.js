import React from 'react';
//import { BrowserRouter as Router, Routes , Route, Navigate } from 'react-router-dom';
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Billing from '../../pages/Billing';
import DetailCommande from '../../pages/DetailCommande';
import DetailProduct from '../../pages/DetailProduct';
import Home from '../../pages/Home';
import Login from '../../pages/Login';
import Success from '../../pages/Success';

const index = () => {
    return (
        

        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home/>} />
                <Route path="/product/:productId" element={<DetailProduct/>} />
                <Route path="/commande/:commandeId" element={<DetailCommande/>} />
                <Route path="/billing" element={<Billing/>} />
                <Route path="/login" element={<Login/>} />
                <Route path="/success" element={<Success/>} />
                <Route
                    path="*"
                    element={<Navigate to="/" replace />}
                />
            </Routes>
        </BrowserRouter>
        
        
    );
};

export default index;