import React from 'react';
import { useSelector } from 'react-redux';
import Product from './Product';
import { isEmpty } from './Utils';


const Shop = () => {

    const posts = useSelector((state) => state.productReducer);
  
    return (
        <>   
           {/* <!-- Page Header Start -->*/}
                <div class="container-fluid bg-secondary mb-5">
                    <div class="d-flex flex-column align-items-center justify-content-center" style={{minHeight: '100px'}}>
                        <h1 class="font-weight-semi-bold text-uppercase mb-3">Our Shop</h1>
                    </div>
                </div>
            {/* <!-- Page Header End -->*/}

            {/*  <!-- Shop Start --> */}
            <div class="container-fluid pt-5">
                <div class="row px-xl-5">
                {/* <!-- Shop Product Start --> */}
                <div class="col-lg-12 col-md-12">
                    <div class="row pb-3">
                        {!isEmpty(posts[0]) &&
                            posts.map((post) => {
                                return <Product post={post} key={post._id} />;
                            }) 
                        }
                    </div>
                </div>
                {/* <!-- Shop Product End --> */}
                </div>
            </div>
            {/* <!-- Shop End --> */}
        </>
    );
};

export default Shop;