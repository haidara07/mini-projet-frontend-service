import React, { useContext } from 'react';
import { UidContext } from "./AppContext";
import Logout from './Log/Logout';

const Navbar = () => {
    const uid = useContext(UidContext);

    return (
        <>
            <div class="container-fluid">
                <div class="row align-items-center py-3 px-xl-5">
                    <div class="col-lg-3 d-none d-lg-block">
                        <a href="/" class="text-decoration-none">
                            <h1 class="m-0 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border px-3 mr-1">M</span>commerce</h1>
                        </a>
                    </div>
                    <a href="/" class="nav-item nav-link active">Shop</a>
                    {!uid ? (
                        <>
                            <a href="/login" class="nav-item nav-link">Login</a>
                        </>
                    ) : (
                        <>
                            <Logout />
                        </>
                    )}
                </div>
            </div>        
        </>
    );
};

export default Navbar;