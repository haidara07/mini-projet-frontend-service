import axios from 'axios';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import Billing from '../pages/Billing';

const ItemCommande = () => {
    const parametres = useParams();
    const [product, setProduct] = useState();
    const [order, setOrder] = useState();
    const [props, setProps] = useState();
    const [billing, setBilling] = useState(false);
    const shipping = 20
    const products = useSelector((state) => state.productReducer);

    axios({
        method: "get",
        url: `${process.env.REACT_APP_API_URL}/order/${parametres.commandeId}`,
        withCredentials: true,  
    })
    .then((res) => {
        setOrder(res.data.order)
        products.forEach(product => {
            if (product._id===order.productId) {
                setProduct(product)
                setProps({
                    shipping:shipping,
                    product:product,
                    order:order
                })
            }
        });        

    })
    .catch((err) => {
        console.log(err);
    }) 

    const handlePayment = () => {
        setBilling(!billing)
    }

    return (
        <>  
            {billing ? (
                <>
                {/* <!-- Page Header Start -->*/}
                <div class="container-fluid bg-secondary mb-5">
                    <div class="d-flex flex-column align-items-center justify-content-center" style={{minHeight: '100px'}}>
                        <h1 class="font-weight-semi-bold text-uppercase mb-3">Billing Address</h1>
                    </div>
                </div>
                {/*<!-- Page Header End -->*/}
            
                {/*  <!-- Checkout Start --> */}
                <div class="container-fluid pt-5">
                    <div class="row px-xl-5">
                        <Billing props={props}/>
                    </div>
                </div>
                </>
            ) : (
            <>

            {/* <!-- Page Header Start -->*/}
            <div class="container-fluid bg-secondary mb-5">
                <div class="d-flex flex-column align-items-center justify-content-center" style={{minHeight: '130px'}}>
                    <h1 class="font-weight-semi-bold text-uppercase mb-3">commande accepté</h1>
                    <div class="d-inline-flex">
                        <p class="m-0">Information de la commande</p>
                    </div>
                </div>
            </div>
            {/*<!-- Page Header End -->*/}
            
            {/*   <!-- Shop Detail Start --> */}
                <div class="container-fluid py-5">
                    <div class="row px-xl-5">

                    {product &&  
                    <>
                        <div class="col-lg-5 pb-5">
                            <div id="product-carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner border">
                                    <div class="carousel-item active">
                                         <img class="w-100 h-100" src={product.images} alt="img"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                     
                        <div class="col-lg-7 pb-5">
                            <h3 class="font-weight-semi-bold">{product.name}</h3>                      
                            <p class="mb-4">{product.description}</p>                            
                            <div class="col-lg-7">
                                <div class="card border-secondary mb-5">
                                    <div class="card-header bg-secondary border-0">
                                        <h4 class="font-weight-semi-bold m-0">Order Total</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between">
                                            <p>{product.name}</p>
                                            <p>${order.totalPrice}</p>
                                        </div>
                                        <hr class="mt-0"/>      
                                        <div class="d-flex justify-content-between">
                                            <h6 class="font-weight-medium">Shipping</h6>
                                            <h6 class="font-weight-medium">${shipping}</h6>
                                        </div>
                                    </div>
                                    <div class="card-footer border-secondary bg-transparent">
                                        <div class="d-flex justify-content-between mt-2">
                                            <h5 class="font-weight-bold">Total</h5>
                                            <h5 class="font-weight-bold">${order.totalPrice+shipping}</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card border-secondary mb-5">                              
                                    <div class="card-footer border-secondary bg-transparent">
                                        <button class="btn btn-lg btn-block btn-primary font-weight-bold my-3 py-3" onClick={handlePayment}>Payer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </>
                        }
                    </div>                
                </div>                        
            {/* <!-- Shop Detail End --> */}
            </>
            ) }  
        </>
    );
};

export default ItemCommande;