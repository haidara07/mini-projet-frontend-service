import React from 'react';
import image from '../styles/success-9pyduc.jpg';

const SuccessComponent = () => {    
    
    return (
        <>   
            {/* <!-- Page Header Start -->*/}
            <div class="container-fluid bg-secondary mb-5">
                <div class="d-flex flex-column align-items-center justify-content-center" style={{minHeight: '200px'}}>
                    <h1 class="font-weight-semi-bold text-uppercase mb-3">Payement Accepté</h1>
                    <div class="d-inline-flex">
                        <h3 class="m-0">Vous allez recevoir un mail de confirmation</h3>
                    </div>
                    <div class="d-inline-flex" style={{marginTop: '15px'}}>
                        <h4 class="m-0">Voir les articles <a href="/">Shop</a></h4>
                    </div>
                </div>
            </div>
            {/*<!-- Page Header End -->*/}

            
            {/*   <!-- Shop Detail Start -->*/}
            <div class="container-fluid py-5">
                <div class="row px-xl-5">
                         
                    <div class="col-lg-5 pb-5" style={{marginLeft:"30%"}}>
                        <div id="product-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner border">
                                <div class="carousel-item active">
                                    <img class="w-100 h-100" src={image} alt="img"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>              
            </div>
            {/* <!-- Shop Detail End -->  */}

        </>
    );
};

export default SuccessComponent;