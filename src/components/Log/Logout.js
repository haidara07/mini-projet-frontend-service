import React from 'react';
import axios from 'axios';
import cookie from "js-cookie";

const Logout = () => {

    const removeCookie = (key) => {
        if (window !== "undefined") {
            cookie.remove(key, { expire:1 });
        }
    }

     const logout = async () => {
        await axios({
            method: 'get',
            url: `${process.env.REACT_APP_API_URL}/user/logout`,
            withCredentials: true,
        })
            .then(() => {
                removeCookie('token');
                window.location = '/';
            })
            .catch((err) => console.log(err));
        window.location= "/"
    }

    return (
        <span onClick={logout} class="nav-item nav-link">Logout</span>
    );
};

export default Logout;