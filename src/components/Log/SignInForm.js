import React, { useState } from 'react';
import axios from "axios";
import { useNavigate } from 'react-router-dom';

const SignInForm = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState('');
    const navigate = useNavigate();

    const handleLogin = (e) => {
        e.preventDefault();

        axios({
            method: "post",
            url: `${process.env.REACT_APP_API_URL}/user/login`,
            withCredentials: true,
            data: {
                email,
                password
            },
        })
        .then((res) => {
            console.log(res)
            navigate(-1)
        })
        .catch((err) => {
            console.log(err);
        })  

    }

    return (
        <>
                <div class="logo">
                    <span>Mcommerce</span>
                </div>
                <h1>Se connecter</h1>

                <form class="flex-c" onSubmit={handleLogin}>
                    <div class="input-box">
                        <div class=" flex-r input">
                            <input 
                                type="text" 
                                placeholder="name@abc.com"
                                onChange={(e) => setEmail(e.target.value)}
                                value={email}
                                required="required"
                            />
                            <i class="fas fa-at"></i>
                        </div>
                    </div>
                    
                    <div class="input-box">
                        <div class="flex-r input">
                            <input 
                                type="password" 
                                placeholder="8+ (a, A, 1, #)"
                                onChange={(e) => setPassword(e.target.value)} 
                                value={password} 
                                required="required"
                            />
                            <i class="fas fa-lock"></i>
                        </div>
                    </div>

                    <input class="btn" type="submit" value="valider"/>
                    <span class="extra-line">
                        <span>Vous n´avez pas de compte?</span>
                    </span>
                </form>

        </>
    );
};

export default SignInForm;