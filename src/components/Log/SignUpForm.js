import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';

const SignUpForm = () => {
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const navigate = useNavigate();


  const handleRegister = async (e) => {
    e.preventDefault()
    
    await axios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}/user/register`,
      withCredentials: true,
      data: {
        "first_name":name,
        "last_name":lastName,
        "email":email,
        "password":password,
      },
    })
      .then((res) => {
        console.log(res.data)
        navigate(-1)    
      })
      .catch((err) => {
        console.log(err);
      });
    };

  return (
    <>
      <div class="logo">
        <span>Mcommerce</span>
      </div>
      <h1>S'inscrire</h1>

      <form class="flex-c" onSubmit={handleRegister}>
        <div class="input-box">
          <div class=" flex-r input">
            <input 
              type="text" 
              placeholder="First name"
              onChange={(e) => setName(e.target.value)}
              value={name}
              required="required"
            />
            <i class="fas fa-at"></i>
          </div>
        </div>

        <div class="input-box">
          <div class=" flex-r input">
                <input
                  type="text" 
                  placeholder="Last name"
                  onChange={(e) => setLastName(e.target.value)}
                  value={lastName}
                  required="required"
                />
                <i class="fas fa-at"></i>
            </div>
        </div>


        <div class="input-box">
            <div class=" flex-r input">
                <input 
                  type="text" 
                  placeholder="name@abc.com"
                  onChange={(e) => setEmail(e.target.value)}
                  value={email}
                  required="required"
                />
                <i class="fas fa-at"></i>
            </div>
        </div>
                    
        <div class="input-box">
            <div class="flex-r input">
                <input 
                  type="password" 
                  placeholder="8+ (a, A, 1, #)"
                  onChange={(e) => setPassword(e.target.value)}
                  value={password}
                  required="required"
                />
                <i class="fas fa-lock"></i>
            </div>
        </div>

        <input class="btn" type="submit" value="valider"/>
        <span class="extra-line">
            <span>Vous avez déjà un compte? </span>
        </span>
      </form>

    </>

  );
};

export default SignUpForm;
