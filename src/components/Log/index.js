import React, {useState} from 'react';
import SignInForm from './SignInForm';
import SignUpForm from './SignUpForm';

const Log = () => {

    const [signUpModal, setSignUpModal] = useState(false);

    const handleModals = (e) => {
        if (e.target.id==="register") {
            setSignUpModal(true);
        }else if(e.target.id==="login"){
            setSignUpModal(false);

        }
    }
    
    return (
        <div class="flex-r login-wrapper">
            {/*<div class="login-text">*/}
            <div class={signUpModal ? "login-text" : "login-text-register"}>

                {signUpModal ? (
                        <>
                        <SignUpForm />
                        <div id='log'>
                            <span id="login" onClick={handleModals}>Se connecter</span>
                        </div>
                        </>

                    ) : (
                        <>
                        <SignInForm />
                        <div id='log'>
                            <span id="register" onClick={handleModals}>S'inscrire</span>
                        </div>
                        </>
                    )
                }

            </div>
		</div>
    );
};

export default Log;