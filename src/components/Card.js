import React from 'react';

const Card = ({ props }) => {

    return (
        <>
        {props &&
        <div class="card border-secondary mb-5">
            <div class="card-header bg-secondary border-0">
                <h4 class="font-weight-semi-bold m-0">Order Total</h4>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <p>{props.product.name}</p>
                    <p>${props.order.totalPrice}</p>
                </div>
                <hr class="mt-0"/>

                <div class="d-flex justify-content-between">
                    <h6 class="font-weight-medium">Shipping</h6>
                    <h6 class="font-weight-medium">${props.shipping}</h6>
                </div>
                </div>
                <div class="card-footer border-secondary bg-transparent">
                <div class="d-flex justify-content-between mt-2">
                    <h5 class="font-weight-bold">Total</h5>
                    <h5 class="font-weight-bold">${props.order.totalPrice+props.shipping}</h5>
                </div>
            </div>
        </div>
        }
        </>
    );
};

export default Card;