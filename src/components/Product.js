import React from 'react';

const Product = ({ post }) => {

    const selectedProduct = () => {
        window.location=`/product/${post._id}`    
    }
    
    return (
        <div class="col-lg-4 col-md-6 col-sm-12 pb-1" onClick={selectedProduct}>
            <div class="card product-item border-0 mb-4">
                <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                    <img 
                        class="img-fluid w-100" 
                        //src="img/product-1.jpg" 
                        src={post.images}
                        alt="img"
                    />
                </div>
                <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                    <h6 class="text-truncate mb-3">
                        {post.name}
                    </h6>
                    <div class="d-flex justify-content-center">
                        <h6>
                            ${post.price}
                        </h6>
                        <h6 class="text-muted ml-2">
                            <del>
                                ${post.cuttedPrice}
                            </del>
                        </h6>
                    </div>
                </div>
                                
            </div>
        </div>
    );
};

export default Product;