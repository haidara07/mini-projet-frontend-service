import React, { useState, useEffect } from 'react';
import Routes from "./components/Routes";
import { UidContext } from './components/AppContext';
import axios from "axios";
import { useDispatch } from 'react-redux';
import { GET_USER } from './actions/user.actions';
import Store from './Store';

function App() {

  const [uid, setUid] = useState(null);
  const dispatch = useDispatch();

  useEffect(() =>{
    const fetchToken = async () => {
      await axios({
        method: "get",
        url:`${process.env.REACT_APP_API_URL}/user/jwtid`,
        withCredentials: true,
      })
      .then((res) => {
        setUid(res.data.userId);
      })
      .catch((err) => console.log("no token"));
    };
    fetchToken();
    
    if(uid) {
      axios
      .get(`${process.env.REACT_APP_API_URL}/user/me`,{ withCredentials: true })
      .then((res) => {
        if (res.data.success===true) {
          dispatch({ type: GET_USER, payload: res.data.user });     
        }else{
          console.log(res)
        }
      })
      .catch((err) => console.log(err));
    }
  }, [uid, dispatch]);

  return (
    
    <UidContext.Provider value={uid}>
      <Store>
          <Routes />
      </Store>
    </UidContext.Provider>  

  );
}

export default App;
