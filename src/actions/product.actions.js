import axios from "axios";

//posts
export const GET_POSTS = "GET_POSTS";

export const getPosts = () => {
    return (dispatch) => {
        return axios
            .get(`${process.env.REACT_APP_API_URL}/product/`)
            .then((res) => {
                const array = res.data.products
                dispatch({ type: GET_POSTS, payload: array});
            })
            .catch((err) => console.log(err))
    }
}
