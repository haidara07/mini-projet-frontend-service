import React from 'react';
import './index.css';
import './styles/css/style.css';
import './styles/login/style.css';
import App from './App';
import { Provider } from 'react-redux';
import { createRoot } from 'react-dom/client';
import { composeWithDevTools } from 'redux-devtools-extension';
import { applyMiddleware, createStore } from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';
import { getPosts } from './actions/product.actions';

const store = createStore(
    rootReducer, composeWithDevTools(applyMiddleware(thunk))
)
store.dispatch(getPosts());

const root = createRoot(document.getElementById("root"));

root.render(
    <Provider  store={store}>
        <App />
    </Provider> 
);
