import React from 'react';
import Navbar from '../components/Navbar';
import Shop from '../components/Shop';

const Home = () => {

    return (
        <>
        <Navbar />
        <Shop />
        </>
    );
};

export default Home;