import React from 'react';
import ItemCommande from '../components/ItemCommande';
import Navbar from '../components/Navbar';

const DetailCommande = () => {

    return (
        <>
        <Navbar />
        <ItemCommande />
        </>
    );
};

export default DetailCommande;