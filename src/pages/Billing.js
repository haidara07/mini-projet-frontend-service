import axios from 'axios';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import Card from '../components/Card';

const Billing = ({ props }) => {
    const userData = useSelector((state) => state.userReducer);
    const [mobile, setMobile] = useState();
    const [address, setAddress] = useState();
    const [zip, setZip] = useState();
    const [cardNumber, setCardNumber] = useState();
    const [code, setCode] = useState();   

    const validateTransaction = () => {
        
        if (!userData.carte) {
            axios({
                method: "put",
                url: `${process.env.REACT_APP_API_URL}/user/me/update`,
                withCredentials: true,
                data:{
                    address: address,
                    mobile: mobile,
                    carte: cardNumber,
                    code: code,
                    zip: zip,
                }
            })
            .then((res) => {
                console.log(res.data)
            })
            .catch((err) => {
                console.log(err);
            })            
        }
        
        axios({
            method: "post",
            url: `${process.env.REACT_APP_API_URL}/payment/`,
            withCredentials: true,
            data:{
                sum: props.order.totalPrice+props.shipping,
                orderId:props.order._id,
                userId:userData._id,
                email:userData.email,
                subject:`Success : payement du produit ${props.product.name}`,
                text : `Le payement du produit <${props.product.name}> a bien été effectué. Montant : <${props.order.totalPrice+props.shipping}> Description du produit : <${props.product.description}> `
            }
        })
        .then((res) => {
            window.location="/success"
        })
        .catch((err) => {
            console.log(err);
        })    
    }

    return (
        <>
             {/*  <!-- Checkout Start --> */}
                    <div class="col-lg-8">
                        <div class="mb-4">
                            <h4 class="font-weight-semi-bold mb-4">Billing Address</h4>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>First Name</label>
                                    <input 
                                        class="form-control" 
                                        type="text" 
                                        placeholder="John"
                                        value={userData.first_name}
                                        disabled
                                    />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Last Name</label>
                                    <input 
                                        class="form-control" 
                                        type="text" 
                                        placeholder="Doe"
                                        value={userData.last_name}
                                        disabled
                                    />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>E-mail</label>
                                    <input 
                                        class="form-control" 
                                        type="text" 
                                        placeholder="example@email.com"
                                        value={userData.email}
                                        disabled
                                    />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Mobile No</label>
                                    <input 
                                        class="form-control" 
                                        type="text" 
                                        placeholder="+123 456 789"
                                        onChange={(e) => setMobile(e.target.value)} 
                                        value={mobile} 
                                        required="required"
                                    />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Address</label>
                                    <input 
                                        class="form-control" 
                                        type="text" 
                                        placeholder="123 Street"
                                        onChange={(e) => setAddress(e.target.value)} 
                                        value={address} 
                                        required="required"
                                    />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>ZIP Code</label>
                                    <input 
                                        class="form-control" 
                                        type="text" 
                                        placeholder="123"
                                        onChange={(e) => setZip(e.target.value)} 
                                        value={zip} 
                                        required="required"
                                    />
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <label>No carte</label>
                                    <input 
                                        class="form-control" 
                                        type="text" 
                                        placeholder="29230909483893409"
                                        onChange={(e) => setCardNumber(e.target.value)} 
                                        value={cardNumber} 
                                        required="required"
                                    />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>code 3 chiffres</label>
                                    <input 
                                        class="form-control" 
                                        type="text" 
                                        placeholder="234"
                                        onChange={(e) => setCode(e.target.value)} 
                                        value={code} 
                                        required="required"
                                    />
                                </div>
                            
                            </div>
                        </div>
                    
                    </div>
                    <div class="col-lg-4">
                            {props && 
                                <Card props={props}/>
                            }
                            
                            <div class="card border-secondary mb-5">
                                
                                <div class="card-footer border-secondary bg-transparent">
                                    <button class="btn btn-lg btn-block btn-primary font-weight-bold my-3 py-3" onClick={validateTransaction}>Valider</button>
                                </div>
                            </div>
                        </div>                     
            {/* <!-- Checkout End --> */}

        </>
    );
};

export default Billing;