import React from 'react';
import SuccessComponent from '../components/SuccessComponent';
import Navbar from '../components/Navbar';

const Success = () => {
    return (
        <>
        <Navbar />
        <SuccessComponent />
        </>
    );
};

export default Success;