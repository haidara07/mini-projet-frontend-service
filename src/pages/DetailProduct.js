import React from 'react';
import ItemDetails from '../components/ItemDetails';
import Navbar from '../components/Navbar';

const DetailProduct = () => {

    return (
        <>
        <Navbar />
        <ItemDetails />
        </>
    );
};

export default DetailProduct;