# => Build container
FROM node:16

WORKDIR /cloudl-client

COPY package.json /cloudl-client/package.json

RUN npm install

COPY . /cloudl-client

EXPOSE 3000

# Start Nginx server
CMD ["npm", "start"]